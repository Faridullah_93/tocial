<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}
$id = $_SESSION['id'];

// $sqlSetting = "SELECT * FROM users WHERE id = $id";
// $stmt = $conn->prepare($sqlSetting);
// $stmt->execute();
// $userData = $stmt->fetchAll(PDO::FETCH_ASSOC);
// foreach($userData as $row){
//   $firstname = $row['firstname'];
//   $lastname = $row['lastname'];
// }

//$sql = "SELECT * FROM post ORDER BY date DESC";
$sqlPost = "SELECT * FROM post INNER JOIN users ON post.user_id = users.id WHERE users.id = $id ORDER BY date DESC";
$stmt = $conn->prepare($sqlPost);
$stmt->execute();
$postData = $stmt->fetchAll(PDO::FETCH_ASSOC);
//var_dump($postData);

if (file_exists('uploadsProfile/'.$id.'.png')){
  $profilePathLog = "uploadsProfile/".$id.".png";
}else{
  $profilePathLog = "uploadsProfile/default.png";
}
?>



<!--user modal-->
<div class="modal fade" id="user_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Choos your profile!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="php/upload.php" method="post" enctype="multipart/form-data">
          <div class="fileBox">
  					<input type="file" style="display:none;" name="userImg" id="userImg" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
  					<label for="userImg" class="userLabel"><i class="far fa-user-circle"></i> <p>Choose a file&hellip;</p></label>
  				</div>
          <div style="margin-top:15px;" >
            <input class="btn btn-success btn-block" type="submit" value="Upload Image" name="submit">
          </div>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

 <!--user row-->
<div class="row">
  <div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">

			<div class="cardbox-heading">
				<!-- START dropdown-->
				<div class="dropdown float-right">
					<button class="btn btn-flat btn-flat-icon" type="button" data-toggle="dropdown" aria-expanded="false">
						<em class="fa fa-ellipsis-h"></em>
					</button>
					<div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu" style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
						<span class="dropdown-item" data-toggle="modal" data-target="#user_modal">New profile photo</span>
						<span class="dropdown-item">hello</span>
						<span class="dropdown-item">hello</span>
						<span class="dropdown-item">hello</span>
					</div>
				</div><!--/ dropdown -->
				
				<div class="media m-0 user_media">
					<div class="d-flex mr-3 user_flex">
						<a href=""><img class="img-fluid rounded-circle user_img" src="<?php echo $profilePathLog; ?>" alt="User"></a>
						  <a><b class="m-0"><?php echo $firstnameLog." ".$lastnameLog ;?></b></a>
					</div>
					<div class="d-flex mr-3 user_flex" style="flex-direction: row;">
						<form method="POST">
						  <input type="hidden" name=""/>
						  <button class="userOption" type="submit" ><i class="fas fa-user-plus"></i></button>
						</form>
						
						<form method="POST">
						  <input type="hidden" name=""/>
						  <button class="userOption" type="submit" ><i class="fas fa-arrow-circle-right"></i></button>
						</form>
						
						<form method="POST">
						  <input type="hidden" name=""/>
						  <button class="userOption" type="submit" ><i class="fas fa-arrow-circle-right"></i></button>
						</form>
						
					</div>
				</div><!--/ media -->
			</div><!--/ cardbox-heading -->

		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div>

<!--new post row-->
<div class="row">	
	<div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">
		   <!--- \\\\\\\Post-->
        <div class="card gedf-card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make a publication</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
              <form method="post" enctype="multipart/form-data">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                        <div class="form-group">
                            <label class="sr-only" for="message">post</label>
                            <textarea class="form-control" id="message" name="message" rows="2" placeholder="What are you thinking?"></textarea>
                        </div>
                    </div>
                </div>
                <div class="btn-toolbar justify-content-between">
                    <div class="btn-group">
                      <input type="file" style="display:none;" name="postFile" id="postImg" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
              				<label for="postImg" class="postImg" ><i class="far fa-image"><p style="display:none;"></p></i></label>
                    </div>
                    
                    <div class="btn-group">
                        <button type="submit" name="share" class="btn btn-primary">share</button>
                    </div>
                </div>
              </form>
            </div>
        </div>
		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div><!--/ row -->


<?php foreach($postData as $post):?>
 <!--post row-->
<div class="row" id="<?php echo $post['post_id'];?>">	
	<div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">
			<div class="cardbox-heading">
				<!-- START dropdown-->
				<div class="dropdown float-right">
					<button class="btn btn-flat btn-flat-icon" type="button" data-toggle="dropdown" aria-expanded="false">
						<em class="fa fa-ellipsis-h"></em>
					</button>
					<div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu" style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
					  <form method="POST" >
						  <input type="hidden" name="followedUser" value="<?php echo $post['user_id'];?>"/>
						  <input type="hidden" name="home" value="home"/>
						  <?php 
						  if($userId != $post['user_id']){
						      echo '<button class="dropdown-item" type="submit" name="followDelete" />Un Follow</button>';
						  }?>
						</form>
						
						<a class="dropdown-item" href="#">Hide post</a>
						<a class="dropdown-item" href="#">Stop following</a>
						<a class="dropdown-item" href="#">Report</a>
					</div>
				</div><!--/ dropdown -->
				<div class="media m-0">
					<div class="d-flex mr-3">
			      <?php if (file_exists('uploadsProfile/'.$post['user_id'].'.png')){
			        $profilePath = "uploadsProfile/".$post['user_id'].".png";
			      }else{
			        $profilePath = "uploadsProfile/default.png";
			      }
			      ?>
			      <a class="serche_item m-0" href="home.php?username=<?php echo $post['username'];?>">
			        <img class="img-fluid rounded-circle" src="<?php echo $profilePath;?>" alt="User">
			      </a>
					</div>
					<div class="media-body">
						<a class="serche_item m-0" href="home.php?username=<?php echo $post['username'];?>">
			        <p class="m-0"><?php echo $post['firstname']." ". $post['lastname'];?></p>
			      </a>
						<small class="postInfo">
						  <span><i class="icon ion-md-pin"></i> Nairobi, Kenya</span>
						  <span><i class="icon ion-md-time"></i> <?php $date = date_create($post['date']); echo date_format($date, "Y-m-d H:i")?></span>
						</small>
					</div>
				</div><!--/ media -->
			</div><!--/ cardbox-heading -->

			<div class="cardbox-item">
			  <p class="message"><?php echo $post['message'] ?></p>
			  
			  <?php
			  $imgP = $post['img'];
			   if (file_exists('uploadsPost/'.$post['img'])){
			     echo '<img class="img-fluid" src="uploadsPost/' .$post['img']. '" >';
			   }
			  ?>
			
			</div><!--/ cardbox-item -->
			<div class="cardbox-base">
				<ul class="float-right">
					<li data-toggle="modal cursor" onclick="showComment('<?php echo $post['post_id'];?>')">
					    <a onclick="<?php if(getComments($post['post_id']) != 0){echo "toggle('comment')";}?>"><i class="fa cursor fa-comments"></i><em class="mr-4"><?php echo getComments($post['post_id']);?></em></a>
					</li>
					<li><a><i class="fa cursor fa-share-alt"></i><em class="mr-3">03</em></a></li>
				</ul>
				<ul>
					
					<?php 
					if(getLikesUser($post['post_id'], $userId) < 1){
					echo '<li class="insertLike" id="'.$post['post_id'].'"><a><i class="fas cursor fa-heart"></i></a></li>';
					}else{
						echo '<li class="deleteLike" id="'.$post['post_id'].'"><a><i class="fas cursor liked fa-heart"></i></a></li>';
					}
					$post_id_likes = $post['post_id'];
					$sql_like_data = " SELECT user_id FROM likes WHERE post_id = '$post_id_likes' LIMIT 3";
					$stmt = $conn->prepare($sql_like_data);
					$stmt->execute();
					$like_data = $stmt->fetchAll(PDO::FETCH_ASSOC);

					?>

					<?php foreach ($like_data as $user_like):
						if(!empty($user_like)):if (file_exists('uploadsProfile/'.$user_like['user_id'].".png")):
					?>
						<li><a href="#"><img src="uploadsProfile/<?php echo $user_like['user_id'].".png";?>" class="img-fluid rounded-circle" alt="User"></a></li>
					<?php  else:?>
						<li><a href="#"><img src="uploadsProfile/default.png" class="img-fluid rounded-circle" alt="User"></a></li>
					<?php endif; endif; endforeach; ?>
					
					<li><a><span><?php echo count(getLikes($post['post_id']));?></span></a></li>
			  	</ul>				   
			</div><!--/ cardbox-base -->
			
			<div class="cardbox-comments">
				<form>
				  <div class="comment-avatar float-left">
					  <img class="rounded-circle" src="<?php echo $profilePathLog;?>" alt="...">                         
					</div>
				  <div class="comment-body">
		            <input class="form-control" type="text" id="comment_<?php echo $post['post_id'];?>" name="comment" placeholder="Write your comment...">
		            <button class="comment" id="<?php echo $post['post_id'];?>" type="button" name="sendComment"><i class="fas fa-arrow-circle-right"></i></button>
		          </div>
				</form>
			</div><!--/ cardbox-like -->
		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div><!--/ row -->
<?php endforeach;?>
  		

