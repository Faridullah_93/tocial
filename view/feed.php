<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}
$user_id_feed = $_SESSION['id'];

$sqlPost = "
SELECT 
	/*post items*/
	post.user_id,
	post.post_id,
	post.message,
	post.img,
	post.date,
	
	/*users items*/
	users.id,
	users.firstname,
	users.lastname,
	users.username,
	users.email
FROM 
	post 
INNER JOIN 
	users ON post.user_id = users.id 
WHERE 
	post.user_id IN (SELECT followed_user FROM follower WHERE following_user = '$user_id_feed') 
OR 
	post.user_id = '$userId' 
ORDER BY date DESC;
";
$stmt = $conn->prepare($sqlPost);
$stmt->execute();
$postData = $stmt->fetchAll(PDO::FETCH_ASSOC);
// var_dump($postData);

?>

<!--new post row-->
<div class="row">	
	<div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">
		   <!--- \\\\\\\Post-->
        <div class="card gedf-card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Make a publication</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
              <form method="post" enctype="multipart/form-data">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
                        <div class="form-group">
                            <label class="sr-only" for="message">post</label>
                            <textarea class="form-control" id="message" name="message" rows="2" placeholder="What are you thinking?"></textarea>
                        </div>
                    </div>
                </div>
                <div class="btn-toolbar justify-content-between">
                    <div class="btn-group">
                      	<input type="file" style="display:none;" name="postFile" id="postImg" class="inputfile inputfile-2"  multiple value="" />
						<label for="postImg" class="postImg" ><i class="far fa-image"></i></label>
                    </div>
                    
                    <div class="btn-group">
                        <button type="submit" name="share" class="btn btn-primary">share</button>
                    </div>
                </div>
              </form>
            </div>
        </div>
		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div><!--/ row -->

<?php foreach($postData as $post):?>
 <!--post row-->
<div class="row" id="<?php echo $post['post_id'];?>">	
	<div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">
			<div class="cardbox-heading">
				<!-- START dropdown-->
				<div class="dropdown float-right">
					<button class="btn btn-flat btn-flat-icon" type="button" data-toggle="dropdown" aria-expanded="false">
						<em class="fa fa-ellipsis-h"></em>
					</button>
					<div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu" style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
					 <!-- <form method="POST" >-->
						<!--  <input type="hidden" name="followedUser" value=""/>-->
						<!--  <input type="hidden" name="home" value="home"/>-->
						 

						<!--</form>-->
						<?php if($userId == $post['user_id']):?>
						<input type="hidden" id="img_<?php echo $post['post_id'];?>" value="<?php echo $post['img'];?>"/>
						<a class="dropdown-item delete_post" id="<?php echo $post['post_id'];?>-delete" >Delete post</a>
						<?php endif;?>
						<a class="dropdown-item" href="#">Hide post</a>
						<?php if($userId != $post['user_id']):?>
						<a class="dropdown-item follow_user" id="<?php echo $post['user_id'];?>">Stop following</a>
						<?php endif;?>
						<a class="dropdown-item" href="#">Report</a>
					</div>
				</div><!--/ dropdown -->
				<div class="media m-0">
					<div class="d-flex mr-3">
			      <?php if (file_exists('uploadsProfile/'.$post['user_id'].'.png')){
			        $profilePath = "uploadsProfile/".$post['user_id'].".png";
			      }else{
			        $profilePath = "uploadsProfile/default.png";
			      }
			      ?>
			      <a class="serche_item m-0" href="home.php?username=<?php echo $post['username'];?>">
			        <img class="img-fluid rounded-circle" src="<?php echo $profilePath;?>" alt="User">
			      </a>
					</div>
					<div class="media-body">
						<a class="serche_item m-0" href="home.php?username=<?php echo $post['username'];?>">
			        <p class="m-0"><?php echo $post['firstname']." ". $post['lastname'];?></p>
			      </a>
						<small class="postInfo">
						  <span><i class="icon ion-md-pin"></i> Nairobi, Kenya</span>
						  <span><i class="icon ion-md-time"></i> <?php $date = date_create($post['date']); echo date_format($date, "Y-m-d H:i")?></span>
						</small>
					</div>
				</div><!--/ media -->
			</div><!--/ cardbox-heading -->

			<div class="cardbox-item">
			  <p class="message"><?php echo $post['message'] ?></p>
			  
			  <?php
			  $imgP = $post['img'];
			   if (file_exists('uploadsPost/'.$post['img'])){
			     echo '<img class="img-fluid" src="uploadsPost/' .$post['img']. '" >';
			   }
			  ?>
			
			</div><!--/ cardbox-item -->
			<div class="cardbox-base">
				<ul class="float-right">
					<li class="cursor <?php if(getComments($post['post_id']) != 0){echo 'comment_button';}?>" id="<?php echo $post['post_id'];?>-comment">
					    <a ><i class="fa cursor fa-comments"></i><em class="mr-4"><?php echo getComments($post['post_id']);?></em></a>
					</li>
					<li><a><i class="fa cursor fa-share-alt"></i><em class="mr-3">03</em></a></li>
				</ul>
				<ul>
					<li class="like_unlike" id="<?php echo $post['post_id'].'-like_unlike';?>" ><a>
						<?php if(getLikesUser($post['post_id'], $userId) < 1):?>
						<i class="fas cursor fa-heart"></i>
						<?php else:?>
						<i class="fas cursor liked fa-heart"></i>
					</a></li>
					
					<?php endif;
					$post_id_likes = $post['post_id'];
					$sql_like_data = " SELECT user_id FROM likes WHERE post_id = '$post_id_likes' LIMIT 3";
					$stmt = $conn->prepare($sql_like_data);
					$stmt->execute();
					$like_data = $stmt->fetchAll(PDO::FETCH_ASSOC);

					?>

					<?php foreach ($like_data as $user_like):
						if(!empty($user_like)):if (file_exists('uploadsProfile/'.$user_like['user_id'].".png")):
					?>
						<li><a href="#"><img src="uploadsProfile/<?php echo $user_like['user_id'].".png";?>" class="img-fluid rounded-circle" alt="User"></a></li>
					<?php  else:?>
						<li><a href="#"><img src="uploadsProfile/default.png" class="img-fluid rounded-circle" alt="User"></a></li>
					<?php endif; endif; endforeach; ?>
					
					<li><a><span><?php echo count(getLikes($post['post_id']));?></span></a></li>
			  	</ul>				   
			</div><!--/ cardbox-base -->
			
			<div class="cardbox-comments">
				<form>
				  <div class="comment-avatar float-left">
					  <img class="rounded-circle" src="<?php echo $profilePathLog;?>" alt="...">                         
					</div>
				  <div class="comment-body">
		            <input class="form-control" type="text" id="comment_<?php echo $post['post_id'];?>" name="comment" placeholder="Write your comment..." autocomplete="off" />
		            <button class="comment" id="<?php echo $post['post_id'];?>" type="button" name="sendComment"><i class="fas fa-paper-plane"></i></button>
		          </div>
				</form>
			</div><!--/ cardbox-like -->
		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div><!--/ row -->
<?php endforeach;?>
  		
  		


