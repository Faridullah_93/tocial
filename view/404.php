<head>
    <meta charset="utf-8">
    <title>Page Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>

        * {
            line-height: 1.2;
            margin: 0;
        }

        html {
            color: #888!important;
            display: table!important;
            font-family: sans-serif!important;
            height: 100%!important;
            text-align: center!important;
            width: 100%!important;
        }

        #404body {
            display: table-cell!important;
            vertical-align: middle!important;
            margin: 2em auto!important;
        }

        #noth1 {
            color: #555!important;
            font-size: 2em!important;
            font-weight: 400!important;
        }

        #404P {
            margin: 0 auto!important;
            width: 280px!important;
        }

        @media only screen and (max-width: 280px) {

            body, p {
                width: 95%;
            }

            h1 {
                font-size: 1.5em;
                margin: 0 0 0.3em;
            }

        }

    </style>
<div id="404body">
    <h1 id="noth1">Page Not Found</h1>
    <p id="404P">Sorry, but the page you were trying to view does not exist.</p>
</div>
<!-- IE needs 512+ bytes: https://blogs.msdn.microsoft.com/ieinternals/2010/08/18/friendly-http-error-pages/ -->
