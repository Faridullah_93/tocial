<?php
session_start();
if (!empty($_SESSION['id'])) {
  header("Location: home.php");
}
include "php/login.php";
include "php/sginup.php";
//echo password_hash('farid', PASSWORD_DEFAULT);
//echo password_hash("root", PASSWORD_DEFAULT);
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
    <!--Custom styles-->
	  <link rel="stylesheet" type="text/css" href="css/login.css">
	  <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>

  <body>
    <div class="limiter">
    		<div class="container-login100">
    			<div class="wrap-login100">
    				<div class="login100-pic js-tilt" data-tilt>
    					<img src="content/user.png" alt="IMG">
    				</div>
    
    				<form method="POST" class="login100-form validate-form">
    					<span class="login100-form-title">
    						Member Login
    					</span>
    
    					<div class="wrap-input100 validate-input">
    						<input class="input100" type="text" name="username" placeholder="Email" autocomplete="username" required>
    						<span class="focus-input100"></span>
    						<span class="symbol-input100">
    							<i class="fas fa-key" aria-hidden="true"></i>
    						</span>
    					</div>
    
    					<div class="wrap-input100 validate-input">
    						<input class="input100" type="password" name="password" placeholder="Password" autocomplete="current-password" required>
    						<span class="focus-input100"></span>
    						<span class="symbol-input100">
    							<i class="fa fa-lock" aria-hidden="true"></i>
    						</span>
    					</div>
    					
    					<div class="container-login100-form-btn">
    						<input type="submit" name="login" class="login100-form-btn" value="Login">
    					</div>
    
    					<div class="text-center p-t-12">
    						<span class="txt1">
    							Forgot
    						</span>
    						<a class="txt2" href="#">
    							Username / Password?
    						</a>
    					</div>
    
    					<div class="text-center p-t-136">
    						<a class="txt2" href="#" data-toggle="modal" data-target="#exampleModalCenter">
    							Create your Account
    							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true" ></i>
    						</a>
    					</div>
    				</form>
    			</div>
    		</div>
    	</div>
    	

      
      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Member Signup</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" class="login100-form validate-form">
                
                <div class="wrap-input100 validate-input">
      						<input class="input100" type="text" name="firstname" placeholder="Firstname" maxlength="80" required>
      						<span class="focus-input100"></span>
      						<span class="symbol-input100">
      							<i class="fas fa-user" aria-hidden="true"></i>
      						</span>
      					</div>
      					
      					<div class="wrap-input100 validate-input">
      						<input class="input100" type="text" name="lastname" placeholder="Lastname" maxlength="80" required>
      						<span class="focus-input100"></span>
      						<span class="symbol-input100">
      							<i class="fas fa-user" aria-hidden="true"></i>
      						</span>
      					</div>
      					
      					<div class="wrap-input100 validate-input">
      						<input class="input100" type="text" name="username" placeholder="Username" maxlength="60" required>
      						<span class="focus-input100"></span>
      						<span class="symbol-input100">
      							<i class="fas fa-key" aria-hidden="true"></i>
      						</span>
      					</div>
      					
      					<div class="wrap-input100 validate-input">
      						<input class="input100" type="email" name="email" placeholder="Email" maxlength="100" required>
      						<span class="focus-input100"></span>
      						<span class="symbol-input100">
      							<i class="fa fa-envelope" aria-hidden="true"></i>
      						</span>
      					</div>
      
      					<div class="wrap-input100 validate-input">
      						<input class="input100" type="password" name="password" placeholder="Password" maxlength="60" autocomplete="current-password" required>
      						<span class="focus-input100"></span>
      						<span class="symbol-input100">
      							<i class="fa fa-lock" aria-hidden="true"></i>
      						</span>
      					</div>
      					
      					<div class="container-login100-form-btn">
      						<input type="submit" name="sginup" class="login100-form-btn" value="sginup">
      					</div>
      					<div class="text-center p-t-136">
      						<a class="txt2" href="#" data-dismiss="modal">
      							Already have one
      							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true" ></i>
      						</a>
      					</div>
      				</form>
            </div>
          </div>
        </div>
      </div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  </body>

</html>
