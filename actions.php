 <?php
//Database connection by using PHP PDO
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}
$userId = $_SESSION['id'];
$userName = $_SESSION['username'];

//loged user profilepathe
if (file_exists('uploadsProfile/'.$userId.'.png')){
  $profilePathLog = "uploadsProfile/".$userId.".png";
}else{
  $profilePathLog = "uploadsProfile/default.png";
}

include "db.php";
include "php/function.php";

if(isset($_POST['action'])){
 //For Load All Data
 if($_POST['action'] == "Load"){
  
  //include "view/feed.php";
   $pages_dir = 'view';
   if (!empty($_REQUEST['page']) || !empty($_REQUEST['user_name'])) {
     $pages = scandir($pages_dir, 0);
     unset($pages[0], $pages[1]);
     
     $page = $_REQUEST['page'];
     $user_name = $_REQUEST['user_name'];
     if (in_array($page.'.php', $pages)) {
      // get user
       include ($pages_dir.'/'.$page.'.php');
     }elseif($userName == $user_name){
        include ($pages_dir.'/setting.php');
     }elseif(!empty($user_name)){
       include ($pages_dir.'/user.php');
     }else{
       include ($pages_dir.'/404.php');
     }
   }else{
     include($pages_dir.'/feed.php');
   }
 }
 // delete post
 if (isset($_POST['action']) == "delete_post" && isset($_POST['post_id'])) {
  $post_id = $_POST['post_id'];
  $post_img = $_POST['post_img'];
  echo $post_img . $post_id;

  $sql = "DELETE FROM post WHERE post_id= $post_id";
  $stmt = $conn->prepare($sql);
  $stmt->execute();
  if ($post_img != "empty") {
      unlink('uploadsPost'.DIRECTORY_SEPARATOR.$post_img);
  }

 }
 //like stmts
 if (isset($_POST['like_post_id']) && $_POST['action'] == "like_unlike") {
 	$post_id = $_POST['like_post_id'];
 	if (getLikesUser($post_id, $userId) < 1) {
  	$sqlLike = "INSERT INTO likes (user_id, post_id) VALUES ('$userId', '$post_id')";
  	$stmt = $conn->prepare($sqlLike);
  	$stmt->execute();
 	}else{
  	$sql_delete = "DELETE FROM likes WHERE user_id='$userId' AND post_id = '$post_id'";
  	$stmt = $conn->prepare($sql_delete);
  	$stmt->execute();
  }
 }
 
 //comment stmt
 if (isset($_POST['comment_post']) && $_POST['action'] == "create_comment") {
  	$post_id = $_POST['comment_post'];
  	$comment = $_POST['comment'];
 	 $sqlComment = "INSERT INTO comment (comment, post_id, user_id) VALUES ('$comment', '$post_id', '$userId')";
  	$stmt = $conn->prepare($sqlComment);
  	$stmt->execute();
 }
 
 //get comments
 if (isset($_POST['post_id_comment']) && $_POST['action'] == "post_comments") {
  $post_id = $_POST['post_id_comment'];
  include "view/comment.php";
 }
 
 // follow and unfollow a user 
 if(isset($_POST['user_to_follow']) && $_POST['action'] == "follow_user"){
  $following_user_input = trim($_POST['user_to_follow']);
  if (getUserFollower($userId, $following_user_input) < 1) {
   $sql_follow = "INSERT INTO follower (following_user, followed_user) VALUES ('$userId', '$following_user_input')";
   $stmt = $conn->prepare($sql_follow);
  	$stmt->execute();
  }else{
   $sql_follow = "DELETE FROM follower WHERE following_user = '$userId' AND followed_user = '$following_user_input'";
   $stmt = $conn->prepare($sql_follow);
  	$stmt->execute();
  }
  	
 }
 //This code for Create new Records
 if($_POST["action"] == "Comment")
 {
  $statement = $connection->prepare("
   INSERT INTO customers (first_name, last_name) 
   VALUES (:first_name, :last_name)
  ");
  $result = $statement->execute(
   array(
    ':first_name' => $_POST["firstName"],
    ':last_name' => $_POST["lastName"]
   )
  );
  if(!empty($result))
  {
   echo 'Data Inserted';
  }
 }

 //This Code is for fetch single customer data for display on Modal
 if($_POST["action"] == "Select")
 {
  $output = array();
  $statement = $connection->prepare(
   "SELECT * FROM customers 
   WHERE id = '".$_POST["id"]."' 
   LIMIT 1"
  );
  $statement->execute();
  $result = $statement->fetchAll();
  foreach($result as $row)
  {
   $output["first_name"] = $row["first_name"];
   $output["last_name"] = $row["last_name"];
  }
  echo json_encode($output);
 }

 if($_POST["action"] == "Update")
 {
  $statement = $connection->prepare(
   "UPDATE customers 
   SET first_name = :first_name, last_name = :last_name 
   WHERE id = :id
   "
  );
  $result = $statement->execute(
   array(
    ':first_name' => $_POST["firstName"],
    ':last_name' => $_POST["lastName"],
    ':id'   => $_POST["id"]
   )
  );
  if(!empty($result))
  {
   echo 'Data Updated';
  }
 }

 if($_POST["action"] == "Delete")
 {
  $statement = $connection->prepare(
   "DELETE FROM customers WHERE id = :id"
  );
  $result = $statement->execute(
   array(
    ':id' => $_POST["id"]
   )
  );
  if(!empty($result))
  {
   echo 'Data Deleted';
  }
 }

}

?>