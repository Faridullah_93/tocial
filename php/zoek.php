<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}


if(isset($_REQUEST["term"])){
	require "../db.php";
    // Prepare a select statement
    $serchInput = $_REQUEST["term"];
		$sqlSerch = "SELECT * FROM `users` WHERE `firstname` LIKE '%$serchInput%' Or `lastname` LIKE '%$serchInput%'";
    $stmt = $conn->prepare($sqlSerch);
    $stmt->execute();
    $serchData = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

?>
<div class="collapse show navbar-collapse" id="searchResult">
  <div class="dropdown-divider"></div>
  <button class="navbar-toggler navbar_icon float-right" type="button" data-toggle="collapse" data-target="#searchResult" aria-controls="searchResult" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-times"></i>
  </button>
  <div class="media m-0 serche_data">
  		<?php if (!empty($serchData)): foreach($serchData as $row): ?>
  		
  			<a class="serche_item m-0" href="home.php?username=<?php echo $row['username'];?>">
	      	<div class="d-flex mr-3 driver">
			  	  <div>
			  	      <?php if (file_exists('../uploadsProfile/'.$row['id'].'.png')){
			  	        $profilePath = "uploadsProfile/".$row['id'].".png";
			  	      }else{
			  	        $profilePath = "uploadsProfile/default.png";
			  	      }
			  	      ?>
			  	      <img class="img-fluid rounded-circle" src="<?php echo $profilePath;?>" alt="User">
			  	 </div>
			  	 <div class="name_form">
				  	<p class="m-0"><?php echo $row['firstname']." ". $row['lastname'];?></p>
					</div>
				</div>
			</a>
	  <?php endforeach; endif;?>
  	</div><!--/ media -->
<!--</div>-->
</div>

