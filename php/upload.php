<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}

// date_default_timezone_set('Europe/London');

// if (date_default_timezone_get()) {
//     echo 'date_default_timezone_set: ' . date_default_timezone_get() . '<br />';
// }

// if (ini_get('date.timezone')) {
//     echo 'date.timezone: ' . ini_get('date.timezone');
// }

// $timeZone = ini_get('date.timezone');

date_default_timezone_set('Europe/Amsterdam');
$date = date("Y-m-d H:i:s");
// echo $date . ' '.$timeZone;
$id = $_SESSION['id'];

if(isset($_POST["submit"]) && !empty($_FILES["userImg"]["name"])){
    
	//If no errors.
	if ($_FILES['userImg']['error']) {
		header("Location: /application");
	}else{
		if (!$_FILES['userImg']['error']) {
	        //Only run if file is below 1 MB.
		    $new_name = '../uploadsProfile/' .$id. '.png';
		    if (imagepng(imagecreatefromstring(file_get_contents($_FILES['userImg']['tmp_name'])), $new_name)) {
				header("Location: /application/user.php");
				echo 'gelukt';
		    } 
	    }
	}

}


// Include the database configuration file
// include 'db.php';
$statusMsg = '';




if(isset($_POST["share"])){
	$message = trim($_POST['message']);
	
	$path = $_FILES['postFile']['name'];
	
	$fileType = pathinfo($path, PATHINFO_EXTENSION);
	
	$fileName = $id.'-'.date("Y-m-dh:i:s").'.'.$fileType;
	
	$targetFilePath = 'uploadsPost/'.$fileName;
	// Upload file to server
	$expensions= array("jpeg","jpg","png","gif");

	if (!empty($_FILES["postFile"]["name"])) {
		if (in_array($fileType,$expensions) === true) {
			if (($_FILES['postFile']['size'] < 20000000)) {
				$upload_file = move_uploaded_file($_FILES["postFile"]["tmp_name"], $targetFilePath);
				if($upload_file){
					$sqlI = "INSERT INTO post (user_id, message, img, date)
					VALUES ('$id', '$message', '$fileName', '$date')";
					$stmt = $conn->prepare($sqlI);
					$stmt->execute();
				}else{
					echo "somthing go wrong!";
				}
			}else{
				echo "to large";
			}
			
		}else{
			echo "this type is not allowed!";
		}
	}elseif(!empty($message)){
		$sqlI = "INSERT INTO post (user_id, message, img, date)
		VALUES ('$id', '$message', 'empty', '$date')";
		$stmt = $conn->prepare($sqlI);
		$stmt->execute();
	}else{
		echo "it is empty";
	}
	
}

// Display status message
echo $statusMsg;
?>

