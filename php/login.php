<?php
session_start();
if(isset($_POST['login'])){
    //sla de gegevens op
    include "db.php";
    $userName = strtolower(trim($_POST['username']));
    $userPwd = trim($_POST['password']);
    //echo $userPwd;
  
    
    //gegevens uit de database
    $sql = "SELECT * FROM users WHERE  `username` = :ph_user  OR  `email` = :ph_user";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(":ph_user", $userName);
    $stmt->execute();
    $userInfo = $stmt->fetch(PDO::FETCH_ASSOC);
    
    //var_dump($userInfo);
    $emailDb = $userInfo['email'];
    $naam = $userInfo['firstname'];
    $userNameDb = $userInfo['username'];
    $pwd = $userInfo['pwd'];
    $id = $userInfo['id'];
    $rol = $userInfo['rol'];
    //var_dump($id);
    //var_dump(password_verify($userPwd, $pwdDb));
    //echo "<br> ". password_hash("root",PASSWORD_DEFAULT);
    
    if ($userNameDb === $userName || $emailDb === $userName  ) {
        if (password_verify($userPwd, $pwd)) {
             $_SESSION['id'] = $id;
             $_SESSION['rol'] = $rol;
             $_SESSION['naam'] = $naam;
             $_SESSION['username'] = $userNameDb;
             //var_dump($_SESSION['id']);
             header('Location: home.php');
        } else {
            echo "
                <div class=\"alert\" style=\"background-color: #f44336;\">
                    <span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span> 
                    <strong>Wrong! </strong> The password and the user name do not match!
                </div>
            ";
        }
    } else {
        echo "
            <div class=\"alert\" style=\"background-color: #f44336;\">
                <span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span> 
                <strong>Wrong! </strong> The password and the user name do not match!
            </div>
        ";
    }
}
?>
