<?php
function getComments($postId){
  include "db.php";
  $sqlC = "SELECT * FROM comment WHERE post_id = '$postId'";
  $stmt = $conn->prepare($sqlC);
  $stmt->execute();
  $comment_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return count($comment_data);
 
}
function getLikes($postId){
  include "db.php";
  $sqlC = "SELECT * FROM likes WHERE post_id = '$postId'";
  $stmt = $conn->prepare($sqlC);
  $stmt->execute();
  $like_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $like_data;
  
}
function getLikesUser($postId, $user_id){
  include "db.php";
  $sqlC = "SELECT * FROM likes WHERE user_id = '$user_id' AND post_id = '$postId'";
  $stmt = $conn->prepare($sqlC);
  $stmt->execute();
  $like_userdata = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return count($like_userdata);
}

function getUserFollower($following_user, $followed_user){
  include "db.php";
  $sqlC = "SELECT * FROM follower WHERE following_user = '$following_user' AND followed_user = '$followed_user'";
  $stmt = $conn->prepare($sqlC);
  $stmt->execute();
  $follow_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return count($follow_data);
}


?>