<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}
$logUser = $_SESSION['id'];
// echo $logUser;
require "../db.php";
if(isset($_REQUEST["comment"])){
    // Prepare a select statement
    
    $post_id = $_REQUEST["comment"];
	  $sqlComment = "
	  SELECT
    	comment.comment_id,
    	comment.comment,
    	/*comment.post_id AS cPost_id,
    	post.message,
    	post.img,
    	post.date,*/
    	comment.user_id,
    	post.post_id,
    	post.user_id     AS user_post,
  
    	users.id,
    	users.firstname,
    	users.lastname,
    	users.username
    FROM 
    	comment
    INNER JOIN 
    	post ON comment.post_id = post.post_id 
    INNER JOIN 
	    users ON comment.user_id = users.id
    WHERE 
    	comment.post_id = '$post_id';
	  ";
    $stmt = $conn->prepare($sqlComment);
    $stmt->execute();
    $comment_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // var_dump($comment_data);
    foreach($comment_data as $cPost){
      $post_id_c = $cPost['post_id'];
    }
}
if (file_exists('../uploadsProfile/'.$logUser.'.png')){
  $logProfile = "uploadsProfile/".$logUser.".png";
}else{
  $logProfile = "uploadsProfile/default.png";
}
?>
<style type="text/css">
.modal-dialog-centered{
    background-color: white;
    min-height: 100%;
    padding:0!important;
}

</style>
<div class="modal-dialog-centered container">
<div class="modal-body">
  <div class="cardbox-base comment_head">
		<ul class="float-right">
			<li class="cursor">
			    <a><i class="far fa-heart"></i></a>
			</li>
			<li onclick="toggle('comment')">
			    <a><i class="fas fa-times cursor"></i></a>
			</li>
		</ul>
		<ul>
	   <li><a><i class="far cursor fa-heart"></i></a></li>
	   <li><a href="#"><img src="http://www.themashabrand.com/templates/bootsnipp/post/assets/img/users/1.jpg" class="img-fluid rounded-circle" alt="User"></a></li>
	   <li><a href="#"><img src="http://www.themashabrand.com/templates/bootsnipp/post/assets/img/users/5.jpg" class="img-fluid rounded-circle" alt="User"></a></li>
	   <li><a href="#"><img src="http://www.themashabrand.com/templates/bootsnipp/post/assets/img/users/2.jpg" class="img-fluid rounded-circle" alt="User"></a></li>
	   <li><a><span>242</span></a></li>
	  </ul>				   
	</div>
  <ul id="comments-list" class="comments-list">
    <?php foreach($comment_data as $comment):?>
		<li>
			<div class="comment-main-level ">
				<!-- Avatar -->
				<?php if (file_exists('../uploadsProfile/'.$comment['user_id'].'.png')){
	        $commentProfile = "uploadsProfile/".$comment['user_id'].".png";
	      }else{
	        $commentProfile = "uploadsProfile/default.png";
	      }
	      ?>
				<div class="comment-avatar float-left">
				  <a href="user.php?username=<?php echo $comment['username'];?>"><img class="rounded-circle comment_img cursor" src="<?php echo $commentProfile;?>" alt="..."></a>                         
				</div>
				<!-- Contenedor del Comentario -->
				<div class="comment-box">
					<div class="comment-head">
						<p class="comment-name by-author"><a href="user.php?username=<?php echo $comment['username'];?>"><?php echo $comment['firstname']." ".$comment['lastname'];?></a></p>
						<i class="fa fa-reply cursor"></i>
						<i class="fa fa-heart cursor"></i>
						<span>hace 20 minutos</span>
					</div>
					<div class="comment-content">
						<p><?php echo $comment['comment'];?></p>
					</div>
				</div>
			</div>
			<!-- Respuestas de los comentarios -->
		</li>
		<?php endforeach;?>
	</ul>
	<div class="cardbox-comments comment_input">
  	<form method="post">
  	  <div class="comment-avatar float-left">
  		  <img class="rounded-circle comment_img" src="<?php echo $logProfile;?>" alt="...">                         
  		</div>
  	  <div class="comment-body comment_input_user">
        <input class="form-control" type="text" name="comment" placeholder="Write your comment..." autocomplete="off">
        <input class="form-control" type="hidden" name="postId" value="<?php echo $post_id_c;?>">
        <button type="submit" name="sendComment"><i class="fas fa-arrow-circle-right"></i></button>
      </div>
  	</form>
  </div>
</div>
</div>

<!--<ul class="comments-list reply-list">-->
<!--  <li>-->
  	<!-- Avatar -->
  
<!--  	<div class="comment-avatar float-left">-->
<!--  	  <img class="rounded-circle" src="" alt="...">                         -->
<!--  	</div>-->
  	<!-- Contenedor del Comentario -->
<!--  	<div class="comment-box">-->
<!--  		<div class="comment-head">-->
<!--  			<h6 class="comment-name"><a href="http://creaticode.com/blog">Lorena Rojero</a></h6>-->
<!--  			<span>hace 10 minutos</span>-->
<!--  			<i class="fa fa-reply"></i>-->
<!--  			<i class="fa fa-heart"></i>-->
<!--  		</div>-->
<!--  		<div class="comment-content">-->
<!--  			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?-->
<!--  		</div>-->
<!--  	</div>-->
<!--  </li>-->
<!--</ul>-->