<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}
$userId = $_SESSION['id'];
require "db.php";
include "php/upload.php";
include "php/update.php";
include "php/delete.php";
require "php/get.php";

if (file_exists('uploadsProfile/'.$userId.'.png')){
  $profilePathLog = "uploadsProfile/".$userId.".png";
}else{
  $profilePathLog = "uploadsProfile/default.png";
}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!--fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body >
    <!--Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light border-bottom">
      <div class="container">
        <button class="navbar-toggler navbar_icon" type="button">
          <a href="home.php"><i class="far fa-images"></i></a>
        </button>
        <!-- droper-->
        <button class="navbar-toggler navbar_icon" type="button" data-toggle="collapse" data-target="#search" aria-controls="search" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fas fa-search"></i>
        </button>
        
        <button class="navbar-toggler navbar_icon" type="button">
          <a href="home.php"><i class="fas fa-users"></i></i></a>
        </button>
        
        <button class="navbar-toggler navbar_icon" type="button" data-toggle="collapse" data-target="#userInfo" aria-controls="userInfo" aria-expanded="false" aria-label="Toggle navigation">
          <i class="fas fa-bars"></i>
        </button>
        
        <div class="collapse navbar-collapse" id="search">
          <div class="dropdown-divider"></div>
          <div class="search-box form-inline my-2 my-lg-0 formSerch">
              <input class="form-control mr-sm-2 nav-serch" type="text" name="serchInput" autocomplete="off" placeholder="Search" aria-label="Search" onkeyup="search(this.value)">
          </div>
          <div id="result" class="searchData"></div>
        </div>

        
        <div class="collapse navbar-collapse none_display" id="userInfo">
          <ul class="navbar-nav ml-auto">
            <div class="dropdown-divider"></div>
            <li class="nav-item dropdown">
              <a class="nav-link " href="home.php?page=setting" >
                 <img class="img-fluid rounded-circle userMenuImg" src="<?php echo $profilePathLog;?>" alt="User"><?php echo " ".$firstnameLog." ".$lastnameLog." ";?>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link " href="php/logout.php" >Logout <i class="fas fa-sign-out-alt"></i></a>
            </li>
          </ul>
        </div>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item ">
               <a class="nav-link" href="home.php" >Home</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <?php echo $firstnameLog." ".$lastnameLog." ";?><img class="img-fluid rounded-circle userMenuImg" src="<?php echo $profilePathLog;?>" alt="User">
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="home.php?page=setting" >My profile</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="php/logout.php" >Logout <i class="fas fa-sign-out-alt"></i></a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--display user search-->
    <div  class="result serche_fluid container-fluid searchDataDesk">
      <div id="resultDesk" class="container search_container"></div>
    </div>
    
  	<div class="container-fluid">
      <?php
  	    //dynamic page
  	    $userName = $_SESSION['username'];
  	    $pages_dir = 'view';
  	    if (!empty($_REQUEST['page']) || !empty($_REQUEST['username'])) {
  	      $pages = scandir($pages_dir, 0);
  	      unset($pages[0], $pages[1]);
  	      
  	      
  	      $page = $_REQUEST['page'];
  	      $userPage = $_REQUEST['username'];
  	      
  	      if (in_array($page.'.php', $pages)) {
  	       // get user
  	        include ($pages_dir.'/'.$page.'.php');
  	      }elseif($userName == $userPage){
            include ($pages_dir.'/setting.php');
  	      }elseif(!empty($userPage)){
  	        include ($pages_dir.'/user.php');
  	      }else{
  	        include ($pages_dir.'/404.php');
  	      }
  	    }else{
  	      include($pages_dir.'/feed.php');
  	    }
  	  ?>
  	</div><!--/ container -->
  	
    <div id="comment" class="modal"></div>
    
    
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Tocial - <?php echo date("Y");?></p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
    <!--custom-->
    <script src="js/script.js"></script>
    <!--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

  </body>

</html>
