 <?php
$hostNaam = "localhost";
$dbNaam = "tocial";
$userNaam = "faridullah";
$pwd = "";

$conn = new PDO("mysql:host=$hostNaam;dbname=$dbNaam",$userNaam,$pwd);
if(!$conn){
    echo "niks";
}

session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}
$userId = $_SESSION['id'];

if(isset($_POST["action"])) //Check value of $_POST["action"] variable value is set to not
{
 //For Load All Data
 if($_POST["action"] == "Load") 
 {
  $sqlPost = "
    SELECT 
    	/*post items*/
    	post.user_id,
    	post.post_id,
    	post.message,
    	post.img,
    	post.date,
    	
    	/*users items*/
    	users.id,
    	users.firstname,
    	users.lastname,
    	users.username,
    	users.email
    FROM 
    	post 
    INNER JOIN 
    	users ON post.user_id = users.id 
    WHERE 
    	post.user_id IN (SELECT followed_user FROM follower WHERE following_user = '$userId') 
    OR 
    	post.user_id = '$userId' 
    ORDER BY date DESC;
    ";
  $stmt = $conn->prepare($sqlPost);
  $stmt->execute();
  $postData = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  $output = '';
  
  if($stmt->rowCount() > 0)
  {
   require "view/feed.php";
  }
 
 }

 //This code for Create new Records
 if($_POST["action"] == "Create")
 {
  $statement = $connection->prepare("
   INSERT INTO customers (first_name, last_name) 
   VALUES (:first_name, :last_name)
  ");
  $result = $statement->execute(
   array(
    ':first_name' => $_POST["firstName"],
    ':last_name' => $_POST["lastName"]
   )
  );
  if(!empty($result))
  {
   echo 'Data Inserted';
  }
 }

 //This Code is for fetch single customer data for display on Modal
 if($_POST["action"] == "Select")
 {
  $output = array();
  $statement = $connection->prepare(
   "SELECT * FROM customers 
   WHERE id = '".$_POST["id"]."' 
   LIMIT 1"
  );
  $statement->execute();
  $result = $statement->fetchAll();
  foreach($result as $row)
  {
   $output["first_name"] = $row["first_name"];
   $output["last_name"] = $row["last_name"];
  }
  echo json_encode($output);
 }

 if($_POST["action"] == "Update")
 {
  $statement = $connection->prepare(
   "UPDATE customers 
   SET first_name = :first_name, last_name = :last_name 
   WHERE id = :id
   "
  );
  $result = $statement->execute(
   array(
    ':first_name' => $_POST["firstName"],
    ':last_name' => $_POST["lastName"],
    ':id'   => $_POST["id"]
   )
  );
  if(!empty($result))
  {
   echo 'Data Updated';
  }
 }

 if($_POST["action"] == "Delete")
 {
  $statement = $connection->prepare(
   "DELETE FROM customers WHERE id = :id"
  );
  $result = $statement->execute(
   array(
    ':id' => $_POST["id"]
   )
  );
  if(!empty($result))
  {
   echo 'Data Deleted';
  }
 }

}

?>

