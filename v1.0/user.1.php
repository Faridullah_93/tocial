<?php
session_start();
if (empty($_SESSION['id'])) {
  header("Location: index.php");
}

$id = $_SESSION['id'];
$user = $_SESSION['username'];

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $username = $_GET['username'];
  // die();
  $sqlPost = "SELECT * FROM post INNER JOIN users ON post.user_id = users.id WHERE users.username = '$username' ORDER BY date DESC";
  $stmt = $conn->prepare($sqlPost);
  $stmt->execute();
  $postData = $stmt->fetchAll(PDO::FETCH_ASSOC);
  //var_dump($postData);
  
  $sql = "SELECT * FROM `users` WHERE `username` = '$username'";
  $stmt = $conn->prepare($sql);
  $stmt->execute();
  $userData = $stmt->fetchAll(PDO::FETCH_ASSOC);
  // var_dump($userData);
  foreach($userData as $row){
    $firstname = $row['firstname'];
    $lastname = $row['lastname'];
    $idUser = $row['id'];
    $usernameFollow = $row['username'];
  }
}

if (file_exists('uploadsProfile/'.$idUser.'.png')){
  $profilePath = "uploadsProfile/".$idUser.".png";
}else{
  $profilePath = "uploadsProfile/default.png";
}


// if (file_exists('uploadsProfile/'.$logedUserId.'.png')){
//   $logedUser = "uploadsProfile/".$logedUserId.".png";
// }else{
//   $logedUser = "uploadsProfile/default.png";
// }

if (file_exists('uploadsProfile/'.$id.'.png')){
  $profilePathLog = "uploadsProfile/".$id.".png";
}else{
  $profilePathLog = "uploadsProfile/default.png";
}

$sqlFollow = "SELECT * FROM follower WHERE following_user = $id AND followed_user = $idUser";
$stmt = $conn->prepare($sqlFollow);
$stmt->execute();
$followData = $stmt->fetch(PDO::FETCH_ASSOC);

?>



  	 
 <!--user row-->
<div class="row">
  <div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">

			<div class="cardbox-heading">
				<!-- START dropdown-->
				<div class="dropdown float-right">
					<button class="btn btn-flat btn-flat-icon" type="button" data-toggle="dropdown" aria-expanded="false">
						<em class="fa fa-ellipsis-h"></em>
					</button>
					<div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu" style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
						<span class="dropdown-item">hello</span>
						<span class="dropdown-item">hello</span>
						<span class="dropdown-item">hello</span>
					</div>
				</div><!--/ dropdown -->
				
				<div class="media m-0 user_media">
					<div class="d-flex mr-3 user_flex">
						<a href=""><img class="img-fluid rounded-circle user_img" src="<?php echo $profilePath;?>" alt="User"></a>
						  <a><b class="m-0"><?php echo $firstname." ".$lastname ;?></b></a>
					</div>
					<div class="d-flex mr-3 user_flex" style="flex-direction: row;">
					  
						<form method="POST" >
						  <input type="hidden" name="followedUser" value="<?php echo $idUser;?>"/>
						  <input type="hidden" name="username" value="<?php echo $usernameFollow;?>"/>
						  <?php
							  if ($idUser == $followData['followed_user']) {
                  echo '<button style="color:black;" class="userOption" type="submit" name="followDelete"><i class="fas fa-wifi"></i></button>';
                }else{
                  echo '<button style="color:gray;" class="userOption" type="submit" name="follow"><i class="fas fa-wifi"></i></button>';
                }
						  ?>
						  
						</form>
						
						<form method="POST">
						  <input type="hidden" name=""/>
						  <button class="userOption" type="submit" ><i class="fas fa-arrow-circle-right"></i></button>
						</form>
						
						<form method="POST">
						  <input type="hidden" name=""/>
						  <button class="userOption" type="submit" ><i class="fas fa-arrow-circle-right"></i></button>
						</form>
						
					</div>
				</div><!--/ media -->
			</div><!--/ cardbox-heading -->

		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div>



<?php foreach($postData as $post):?>
 <!--post row-->
<div class="row">	
	<div class="col-lg-6 offset-lg-3">
		<div class="cardbox shadow-lg bg-white">

			<div class="cardbox-heading">
				<!-- START dropdown-->
				<div class="dropdown float-right">
					<button class="btn btn-flat btn-flat-icon" type="button" data-toggle="dropdown" aria-expanded="false">
						<em class="fa fa-ellipsis-h"></em>
					</button>
					<div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu" style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
					  <form method="POST"> 
					    <input type="hidden" name="postId" value="<?php echo $post['postId'];?>"/>
					    <input type="hidden" name="postImg" value="<?php echo $post['img'];?>"/>
					    <input class="dropdown-item" type="submit" name="postDelete" value="Delete"/>
					  </form>
						<a class="dropdown-item" href="#">Delete this</a>
						<a class="dropdown-item" href="#">Stop following</a>
						<a class="dropdown-item" href="#">Report</a>
					</div>
				</div><!--/ dropdown -->
				<div class="media m-0">
					<div class="d-flex mr-3">
					 
						<a href=""><img class="img-fluid rounded-circle" src="<?php echo $profilePath; ?>" alt="User"></a>
						<!--<a href=""><img class="img-fluid rounded-circle" src="uploadsProfile/<?php echo $post['user_id'];?>.png" alt="User"></a>-->
					</div>
					<div class="media-body">
						<p class="m-0"><?php echo $post['firstname']." ". $post['lastname'];?></p>
						
						<small class="postInfo">
						  <span><i class="icon ion-md-pin"></i> Nairobi, Kenya</span>
						  <span><i class="icon ion-md-time"></i> <?php $date = date_create($post['date']); echo date_format($date, "Y-m-d H:i")?></span>
						</small>
					</div>
				</div><!--/ media -->
			</div><!--/ cardbox-heading -->

			<div class="cardbox-item">
			  <p class="message"><?php echo $post['message'] ?></p>
			  <?php
			  $imgP = $post['img'];
			   if (file_exists('uploadsPost/'.$post['img'])){
			     echo '<img class="img-fluid" src="uploadsPost/' .$post['img']. '" >';
			   }
			  ?>
			</div><!--/ cardbox-item -->
			<div class="cardbox-base">
				<ul class="float-right">
					<li><a><i class="fa fa-comments"></i></a></li>
					<li><a><em class="mr-5">12</em></a></li>
					<li><a><i class="fa fa-share-alt"></i></a></li>
					<li><a><em class="mr-3">03</em></a></li>
				</ul>
				<ul>
					<li><a><i class="fa fa-thumbs-up"></i></a></li>
					<li><a><span>242 Likes</span></a></li>
				</ul>			   
			</div><!--/ cardbox-base -->
			<div class="cardbox-comments">
				<div class="comment-avatar float-left">
				  <img class="rounded-circle" src="<?php echo $profilePathLog;?>" alt="...">                         
				</div>
				<form>
				  <div class="comment-body">
            <input class="form-control" type="text" placeholder="Write your comment...">
            <button type="submit" ><i class="fas fa-arrow-circle-right"></i></button>
          </div>
				</form>
			</div><!--/ cardbox-like -->

		</div><!--/ cardbox -->
	</div><!--/ col-lg-6 -->	
</div><!--/ row -->
<?php endforeach;?>
  		