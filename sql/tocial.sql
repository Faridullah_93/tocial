-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Genereertijd: 11 apr 2019 om 08:14
-- Serverversie: 5.5.57-0ubuntu0.14.04.1
-- PHP-versie: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `tocial`
--
CREATE DATABASE IF NOT EXISTS `tocial` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tocial`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `FK` (`post_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128 ;

--
-- Gegevens worden uitgevoerd voor tabel `comment`
--

INSERT INTO `comment` (`comment_id`, `comment`, `post_id`, `user_id`) VALUES
(6, 'waw', 39, 1),
(7, 'hay waw', 39, 1),
(8, 'dat is mooie ', 39, 1),
(9, 'jhjshajshaj', 39, 1),
(10, 'asasas', 39, 1),
(11, 'asasasasasa', 39, 1),
(12, 'helllo', 40, 3),
(13, 'joboy', 40, 3),
(14, 'hoi', 55, 1),
(15, 'hay waw', 55, 1),
(16, 'dat is mooie ', 55, 1),
(17, 'jhjshajshaj', 55, 1),
(18, 'dat is mooie ', 55, 1),
(19, 'beatie', 54, 1),
(21, 'haloooo', 54, 1),
(22, 'helooo', 54, 1),
(23, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 54, 1),
(24, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 54, 1),
(25, 'Waw', 56, 4),
(26, 'Waw', 56, 4),
(27, 'Waw', 56, 1),
(28, 'helooo', 56, 4),
(29, 'dat is mooie ', 61, 1),
(33, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 54, 4),
(34, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 59, 4),
(35, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 64, 1),
(36, 'Mashallah', 64, 1),
(37, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 54, 1),
(38, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 64, 1),
(39, 'Somra khaista zargyaaa', 54, 4),
(40, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 54, 1),
(41, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 64, 1),
(42, 'dat is mooie ', 64, 1),
(43, 'waw', 62, 4),
(44, 'dat is mooie ', 67, 4),
(45, 'jhjshajshaj', 65, 2),
(46, 'wat', 69, 1),
(47, 's', 69, 1),
(48, 's', 69, 1),
(49, 'It is required!', 68, 1),
(50, 'Slaam', 69, 4),
(51, 'It is required!', 68, 1),
(52, 'It is required!', 68, 1),
(53, 'It is required!', 64, 1),
(54, 'It is required!', 64, 1),
(55, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 69, 1),
(56, 'It is required!', 59, 1),
(57, 'jo ', 68, 1),
(58, 'It is required!', 60, 1),
(59, 'addddd', 69, 1),
(60, 'ada', 69, 1),
(61, 'adasdsad', 69, 1),
(62, 'sdasd', 69, 1),
(63, 'asdadas', 69, 1),
(64, 'dsds', 69, 1),
(65, 'helooooooo', 69, 1),
(66, 'dd', 70, 1),
(67, 'zargyaa', 67, 1),
(68, 'a', 70, 1),
(69, 'It is required!', 70, 1),
(70, 'It is required!', 70, 1),
(71, 'It is required!', 70, 1),
(72, 'It is required!', 70, 1),
(73, '', 70, 1),
(74, 'sfdfdf', 70, 1),
(75, 'sfdfdf', 70, 1),
(76, 'sfdfdf', 70, 1),
(77, 'sfdfdf', 70, 1),
(78, 'It is required!', 70, 1),
(79, 'It is required!', 70, 1),
(80, 'It is required!', 70, 1),
(81, 'It is required!', 70, 1),
(82, '', 70, 1),
(83, 'It is required!', 70, 1),
(84, 'It is required!', 70, 1),
(85, 'It is required!', 70, 1),
(86, '', 70, 1),
(87, '', 70, 1),
(88, 'sas', 70, 1),
(89, 'asa', 69, 1),
(90, 'asa', 69, 1),
(91, 'asa', 69, 1),
(92, 'asa', 69, 1),
(93, 'asa', 69, 1),
(94, 'asa', 69, 1),
(95, 'sasas', 70, 1),
(96, 'saassaa', 67, 1),
(97, 'sassasa', 67, 1),
(98, '', 68, 1),
(99, '', 68, 1),
(100, 'sss', 70, 1),
(101, '', 70, 1),
(102, 'hellllooo', 70, 1),
(103, 'aa', 70, 1),
(104, 'aaaaaaaaaaaaaaaaaaaaaaaaa', 70, 1),
(105, 'aaa', 70, 1),
(106, 'aa', 70, 1),
(107, 'Waw', 63, 1),
(108, 'Ù…Ø§Ø´Ø§Ø§Ù„Ù„Ù‡', 66, 1),
(109, 'waw', 66, 1),
(110, 'olam', 72, 1),
(111, 'slaam', 66, 1),
(112, 'wslaam', 2, 1),
(113, 'Waw', 1, 1),
(114, 'Waw', 1, 1),
(115, 'Waw', 1, 1),
(116, 'Slaaam', 1, 1),
(117, 'Slaaa', 1, 1),
(118, 'Sjshhsjsjs', 1, 1),
(119, 'Slaaam', 1, 1),
(120, 'Waw', 1, 4),
(121, 'Sssss', 1, 4),
(122, 'Hell', 13, 4),
(123, 'Ø³Ù„Ø§Ø§Ù…', 23, 1),
(124, 'ÙˆÙˆÙˆ', 26, 1),
(125, 'helloooo', 44, 1),
(126, 'It is required!', 46, 1),
(127, 'Oops', 46, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `follower`
--

CREATE TABLE IF NOT EXISTS `follower` (
  `follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `following_user` int(11) NOT NULL,
  `followed_user` int(11) NOT NULL,
  PRIMARY KEY (`follow_id`),
  KEY `following_user` (`following_user`),
  KEY `followed_user` (`followed_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Gegevens worden uitgevoerd voor tabel `follower`
--

INSERT INTO `follower` (`follow_id`, `following_user`, `followed_user`) VALUES
(1, 4, 3),
(2, 2, 1),
(3, 2, 3),
(4, 2, 4),
(5, 4, 1),
(6, 1, 8),
(9, 3, 4),
(25, 5, 1),
(33, 1, 3),
(37, 1, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `like_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`like_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=443 ;

--
-- Gegevens worden uitgevoerd voor tabel `likes`
--

INSERT INTO `likes` (`like_id`, `user_id`, `post_id`) VALUES
(80, 4, 63),
(81, 4, 62),
(82, 4, 61),
(83, 4, 60),
(84, 4, 59),
(123, 4, 54),
(157, 4, 64),
(161, 4, 65),
(163, 4, 66),
(166, 3, 67),
(169, 3, 66),
(170, 2, 67),
(171, 2, 66),
(172, 2, 65),
(173, 2, 64),
(174, 2, 63),
(175, 2, 62),
(176, 2, 61),
(177, 2, 60),
(178, 2, 54),
(179, 2, 59),
(180, 6, 65),
(181, 6, 63),
(185, 5, 65),
(187, 5, 61),
(188, 5, 60),
(190, 5, 59),
(191, 5, 54),
(205, 1, 67),
(210, 5, 64),
(211, 5, 63),
(212, 5, 62),
(228, 5, 69),
(230, 5, 68),
(261, 4, 68),
(262, 4, 67),
(263, 4, 69),
(301, 1, 65),
(303, 1, 63),
(308, 1, 64),
(340, 1, 68),
(347, 1, 69),
(358, 1, 60),
(359, 1, 70),
(360, 1, 62),
(361, 1, 54),
(362, 1, 59),
(365, 1, 71),
(367, 1, 61),
(368, 1, 72),
(378, 1, 73),
(384, 1, 0),
(390, 1, 66),
(392, 1, 2),
(404, 1, 1),
(405, 1, 3),
(406, 4, 1),
(407, 4, 3),
(408, 4, 17),
(410, 4, 16),
(411, 4, 15),
(412, 4, 13),
(413, 4, 19),
(417, 4, 20),
(418, 1, 20),
(419, 1, 15),
(420, 1, 22),
(421, 1, 18),
(422, 1, 23),
(425, 1, 26),
(426, 4, 26),
(427, 1, 28),
(428, 1, 31),
(430, 1, 40),
(431, 1, 42),
(432, 1, 39),
(435, 1, 36),
(436, 1, 49),
(438, 1, 44),
(439, 1, 38),
(440, 1, 46),
(441, 1, 48),
(442, 1, 45);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Gegevens worden uitgevoerd voor tabel `post`
--

INSERT INTO `post` (`post_id`, `user_id`, `message`, `img`, `date`) VALUES
(36, 1, '', '1-2019-02-2501:48:26.jpg', '2019-02-25 13:48:26'),
(38, 1, '', '1-2019-02-2501:49:35.jpg', '2019-02-25 13:49:35'),
(39, 1, '', '1-2019-02-2501:54:46.jpg', '2019-02-25 13:54:46'),
(40, 1, '', '1-2019-02-2501:57:42.jpg', '2019-02-25 13:57:42'),
(44, 1, 'djsd', 'empty', '2019-02-28 10:23:49'),
(45, 1, '', '1-2019-02-2810:24:16.jpg', '2019-02-28 10:24:16'),
(46, 1, 'hj', 'empty', '2019-02-28 10:51:16'),
(48, 1, '', '1-2019-02-2810:51:43.jpg', '2019-02-28 10:51:43');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(80) NOT NULL,
  `lastname` varchar(80) NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(60) NOT NULL,
  `rol` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserName` (`username`,`email`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Gegevens worden uitgevoerd voor tabel `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `email`, `pwd`, `rol`) VALUES
(1, 'Faridullah', 'Worbal', 'farid', 'farid@hotmail.com', '$2y$10$NvXjlXozk7pDv5.40w7ZHO6JQRnGv4hutMt/ADjWfBHWHZz5yYQ5W', 'Admin'),
(2, 'Omar', 'Worbal', 'khan', 'faridullahworbal1998@gmail.com', '$2y$10$ui2QkOXOeGQfusnof0gnHegly79/66sTwMM7jCrBNJGhpgggd61XW', ''),
(3, 'Afghan', 'Worbal', 'afghan', 'faridullahworbal1998@gmail.com', '$2y$10$yvqXBwkXQ3roXeT8O5XGPu.2Zaw.YAJ.pWYquwEAjXDUp/AY0NLUC', ''),
(4, 'Marghalera', 'Farid', 'marghalera', 'faridullah19989@hotmail.com', '$2y$10$p1nYDNvVKDcH365Vj0iq2OkVObJQG3rtqk.Nqz.r11c8qOqbY5oNa', ''),
(5, 'Mansoor', 'Khorshid', 'mansoor', 'mansoorr@hotmail.com', '$2y$10$gfmy3yid1bL2Cb3tGx9yUOi33fPKLNssElHp7C2stfreMh7987Fa2', ''),
(6, 'Khaiber', 'Khorshid', 'khaiber', 'khaiber@hotmail.com', '$2y$10$upN9Hw8V54nWETqbu.fz.uwi4ZUCLKRvv2eKCOtVGtkmcQUD1oqMe', ''),
(7, 'Helmand', 'Khorshid', 'helmand', 'helmand@hotmail.com', '$2y$10$MksM0UubEXSAfRwwD53pkuIPCCrSZ69ei.W3tb6v.3ILOW92i3qgW', ''),
(8, 'Toragha', 'Adil', 'adil', 'adil@hotmail.com', '$2y$10$/tMOmxTkL3GWimnuH7UE..IlYVzL16JvrVdEw1GXw/84gHhJnZ2YK', '');

--
-- Beperkingen voor gedumpte tabellen
--

--
-- Beperkingen voor tabel `follower`
--
ALTER TABLE `follower`
  ADD CONSTRAINT `follower_ibfk_1` FOREIGN KEY (`following_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `follower_ibfk_2` FOREIGN KEY (`followed_user`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
