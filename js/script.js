// /* When the user clicks on the button, 
// toggle between hiding and showing the dropdown content */
// function userDrop() {
//     document.getElementById("myDropdown").classList.toggle("show");
// }

// // Close the dropdown if the user clicks outside of it
// window.onclick = function(e) {
//   if (!e.target.matches('.dropbtn')) {
//     var myDropdown = document.getElementById("myDropdown");
//       if (myDropdown.classList.contains('show')) {
//         myDropdown.classList.remove('show');
//       }
//   }
// }

/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

// 'use strict';

// ;( function ( document, window, index )
// {
	
// }( document, window, 0 ));


// $(document).click(function(e) {
// 	if (!$(e.target).is('.panel-body')) {
//     	$('.collapse').collapse('hide');	    
//     }
// });


// search

// $(document).ready(function(){
//     $('.search-box input[type="text"]').on("keyup input", function(){
//         /* Get input value on change */
//         var inputVal = $(this).val();
//         // var resultDropdown = $(this).siblings("#result");
//         if(inputVal.length){
//             $.get("php/zoek.php", {term: inputVal}).done(function(data){
//                 // Display the returned data in browser
//                 //resultDropdown.html(data);
//                 document.getElementById("result").innerHTML = data;
//                 document.getElementById("resultDesk").innerHTML = data;
//             });
//         } else{
//             document.getElementById("result").innerHTML = '';
//             document.getElementById("resultDesk").innerHTML = '';
//         }
//     });
// });

// zoek
function search(str) {
  var xhttp;
  if (str.length == 0) { 
    document.getElementById("resultDesk").innerHTML = "";
    document.getElementById("result").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("result").innerHTML = this.responseText;
      document.getElementById("resultDesk").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "php/zoek.php?term="+str, true);
  xhttp.send();   
}
// get comments
function showComment(str) {
  var xhttp;
  if (str.length == 0) { 
    document.getElementById("comment").innerHTML = "";
    return;
  }
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("comment").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "view/comment.php?comment="+str, true);
  xhttp.send();   
}

// function showLikes(str) {
//   var xhttp;
//   if (str.length == 0) { 
//     document.getElementById("comment").innerHTML = "";
//     return;
//   }
//   xhttp = new XMLHttpRequest();
//   xhttp.onreadystatechange = function() {
//     if (this.readyState == 4 && this.status == 200) {
//         xhttp.open("GET", "php/update.php?like="+str, true);
//         xhttp.send(); 
//     }
   
//   };
    
// }


// $(document).ready(function(){

 // function insertLike(postid) {
  
//   var like_post_id = postid;
// // AJAX code to send data to php file.
//   $.ajax({
//       type: "POST",
//       url: "php/update.php",
//       data: {like_post_id:like_post_id},
//       dataType: "JSON",
//       success: function(data) {
//       fetchUser();
//        // $("#message").html(data);
//        // $("p").addClass("alert alert-success");
//       }
//   });
// }


// });

// if( document.getElementById("postImg").files.length == 0 ){
//     console.log("no files selected");
// }



// toggler
// function toggle(id) {
//    document.getElementById(id).classList.toggle("show");
// }



$(document).ready(function(){
 $(document).on('click', '#remove_modal', function(){
   $('#comment_modal').removeClass("show"); 
   $('#body').removeClass("show_modal");
   $('#comment_modal').html("");
   getData();
 });
 
//  var yourNavigation = $(".nav");
//     stickyDiv = "sticky";
//     yourHeader = $('.header').height(); 
// $(window).scroll(function() {
//   if( $(this).scrollTop() > yourHeader ) {
//     yourNavigation.addClass(stickyDiv);
//   } else {
//     yourNavigation.removeClass(stickyDiv);
//   }
// });
 // var imgVal = $('#postImg').val(); 
 // if(imgVal=='') 
 // { 
 //     alert("empty input file"); 
 
 // } 
 // else
 //  {
 //    alert('file fill in');
 //  }
//  function toggle_comments(id) {
//    document.getElementById(id).classList.toggle("show");
//    document.getElementById("body").classList.toggle("show_modal");
// }
 
 getData(); //page dat laden
 function getData() // in dev #post_rsult
 {
  var page = $('#page_name').attr("value");
  var user_name = $('#user_name_page').attr("value");
  var action = "Load";
  $.ajax({
   "processing":true,
   "serverSide":true,
   url : "actions.php", 
   method:"POST", 
   data:{action:action, page:page, user_name:user_name}, 
   success:function(data){
    $('#result_post').html(data); 
   }
  });
  

 }


 // like and unlike function
 $(document).on('click', '.like_unlike', function(){ //insert like
  var str_like = $(this).attr("id");
  var like_post_id = str_like.split("-", 1).toString();
  var action = "like_unlike";
  $.ajax({
   url:"actions.php",
   method:"POST",
   data:{like_post_id:like_post_id, action:action},
   success:function(data)
   {
    getData();
   }
  })
 });
 
 //  follow and unfollow function
  $(document).on('click', '.follow_user', function(){
   var str_user =  $(this).attr("id"); 
   var user_to_follow = str_user.split("-", 1).toString();
   var action = "follow_user";
   $.ajax({
    url:"actions.php",
    method:"POST",
    data:{user_to_follow:user_to_follow, action:action },
    success:function(data)
    {
     getData();
    }
   })
  });
  
  $(document).on('click', '.delete_post', function(){// delete one post
   var str_post =  $(this).attr("id"); 
   var post_id = str_post.split("-", 1).toString();
   var post_img = $('#img_'+post_id).val();
   var action = "delete_post";
   
   swal({
     title: "Are you sure?",
     text: "Once deleted, you will not be able to recover this post!",
     icon: "warning",
     buttons: [true, "Delete it!"],
     dangerMode: true,
   })
   .then((willDelete) => {
     if (willDelete) {
       swal("Poof! Your post has been deleted!", {
         icon: "success",
       });
      // Standard confirm
       $.ajax({
        url:"actions.php",
        method:"POST",
        data:{post_id:post_id, post_img:post_img, action:action },
        success:function(data)
        {
         getData();
        }
       })
     } 
   });
  });


 $(document).on('click', '.comment', function(){ // insert comment for an post
  var comment_post = $(this).attr("id");
  if($('#comment_'+comment_post).val() != ''){
   var comment = $('#comment_'+comment_post).val();
  }else{
   var comment = $('#comment_input_'+comment_post).val();
  } 
  var action = "create_comment";
  
  if(comment != '' && comment != undefined) //check if the comment is not empty
  {
   $.ajax({
    url : "actions.php",    
    method:"POST",    
    data:{comment_post:comment_post, comment:comment, action:action}, 
    success:function(data){
     getData();    // page refrshing
    }
   });
  }else{
   $('#comment_'+comment_post).css("border" ,"2px solid red ");
   $('#comment_'+comment_post).val("It is required!"); 
   
   $('#comment_input_'+comment_post).css("border" ,"2px solid red ");
   $('#comment_input_'+comment_post).val("It is required!");
   window.setTimeout(function(){
       getData(); 
    }, 1000);
  }
 });
 

 $(document).on('click', '.comment_button', function(){// get comments of one post
   var str_comment =  $(this).attr("id"); 
   var post_id_comment = str_comment.split("-", 1).toString();
   var action = "post_comments";
  
   $.ajax({
    url:"actions.php",
    method:"POST",
    data:{post_id_comment:post_id_comment, action:action },
    success:function(data)
    {
     $('#comment_modal').html(data); 
     $('#comment_modal').addClass("show"); 
     $('#body').addClass("show_modal");
    }
   })
  });
});